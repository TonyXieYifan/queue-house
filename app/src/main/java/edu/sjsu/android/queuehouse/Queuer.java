package edu.sjsu.android.queuehouse;

import android.os.Parcel;
import android.os.Parcelable;

public class Queuer implements Parcelable {
    private String name;
    private int image;
    private String place;
    private String rank;
    private String price;
    private String description;
    private String hours;
    private String cost;


    public Queuer(String name, int image, String place, String rank, String price, String description){
        this.name = name;
        this.image = image;
        this.place = place;
        this.rank = rank;
        this.price = price;
        this.description=description;
    }

    public Queuer(String name, int image, String hours, String cost){
        this.name = name;
        this.image = image;
        this.hours = hours;
        this.cost = cost;

    }

    protected Queuer(Parcel in) {
        name = in.readString();
        image = in.readInt();
        place = in.readString();
        price = in.readString();
        rank = in.readString();
        description = in.readString();
    }

    public static final Creator<Queuer> CREATOR = new Creator<Queuer>() {
        @Override
        public Queuer createFromParcel(Parcel in) {
            return new Queuer(in);
        }

        @Override
        public Queuer[] newArray(int size) {
            return new Queuer[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getImage(){
        return image;
    }

    public String getPlace(){
        return place;
    }
    public String getRank(){
        return rank;
    }
    public String getPrice(){
        return price;
    }
    public String getDescription(){
        return description;
    }

    public String getHours(){
        return hours;
    }
    public String getCost(){
        return cost;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(image);
        dest.writeString(place);
        dest.writeString(price);
        dest.writeString(rank);
        dest.writeString(description);
    }
}

