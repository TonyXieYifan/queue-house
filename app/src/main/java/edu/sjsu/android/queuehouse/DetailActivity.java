package edu.sjsu.android.queuehouse;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    private ImageView d_image;
    private TextView d_name;
    private TextView description;
    private NumberPicker numberPicker;
    private int price;
    private Uri uri = OrdersProvider.CONTENT_URI;
    private int balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        d_image = findViewById(R.id.d_image);
        d_name = findViewById(R.id.d_name);
        description = findViewById(R.id.description);
        Queuer item = getIntent().getParcelableExtra("current");
        d_name.setText(item.getName());
        d_image.setImageResource(item.getImage());
        description.setText(item.getDescription());
        numberPicker=findViewById(R.id.numberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(12);
        price = Integer.valueOf(item.getPrice().replaceAll("[^\\d.]", ""));
        System.out.println(price);
    }

    public void placeOrder(View view){
        AlertDialog dialog = new AlertDialog.Builder(DetailActivity.this)
                .setMessage("The total price is "+price*numberPicker.getValue()+"$")
                .setPositiveButton("Wait!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton("Place it!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try (Cursor c = getContentResolver().query(UserInfoProvider.CONTENT_URI, null, null, null, "name")) {
                            if (c.moveToFirst()) {
                                do {
                                    if(c.getString(0).equals(SignInActivity.userPhone)){
                                        balance=Integer.valueOf(c.getString(3));
                                    }
                                } while (c.moveToNext());
                            }
                        }
                        if(balance<price*numberPicker.getValue()){
                            Toast.makeText(DetailActivity.this, "Balance is not enough!", Toast.LENGTH_LONG).show();
                        }else {
                            SharedPreferences sp = getSharedPreferences("userID", Context.MODE_PRIVATE);
                            String phone = sp.getString("phone", null);
                            ContentValues values = new ContentValues();
                            values.put(OrdersDB.USERID, phone);
                            values.put("name", d_name.getText().toString());
                            values.put("hours", numberPicker.getValue());
                            values.put("cost", Integer.toString(price * numberPicker.getValue()));
                            if (getContentResolver().insert(uri, values) != null) {
                                Toast.makeText(DetailActivity.this, "Order Successfully!", Toast.LENGTH_SHORT).show();
                            }
                            SharedPreferences sp1 = getSharedPreferences("userID", MODE_PRIVATE);
                            String phone1 = sp1.getString("phone", null);
                            ContentValues cv = new ContentValues();
                            cv.put("balance", String.valueOf(balance-(price*numberPicker.getValue())));
                            getContentResolver().update(UserInfoProvider.CONTENT_URI, cv, null, new String[]{phone1});
                        }
                    }
                })
                .show();

    }
}