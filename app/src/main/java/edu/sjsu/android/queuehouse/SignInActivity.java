package edu.sjsu.android.queuehouse;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignInActivity extends UIActivity{

    private SharedPreferences sp;
    private SharedPreferences user;
    private String account;
    private String pw;
    public static String userPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        Button logIn = findViewById(R.id.logIn);
        EditText accountInput = findViewById(R.id.account);
        EditText pwInput = findViewById(R.id.password);
        UserInfoDB db = new UserInfoDB(SignInActivity.this);

        sp = getPreferences(MODE_PRIVATE);
        if(sp.contains("account")){
            ((CheckBox)findViewById(R.id.checkBox)).setChecked(true);
            accountInput.setText(sp.getString("account", ""));
            pwInput.setText(sp.getString("password", ""));
        }

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                account = accountInput.getText().toString();
                userPhone=account;
                pw = pwInput.getText().toString();
                if(db.isUserExisted(account, pw)){
                    logIn();
                }else{
                    Toast.makeText(SignInActivity.this, "Invalid Account Info", Toast.LENGTH_LONG).show();
                }
            }
        });

        TextView other = findViewById(R.id.other);
        String text = getString(R.string.other);
        SpannableString ss = new SpannableString(text);
        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Uri log = Uri.parse("http://www.google.com");
                Intent implicit = new Intent(Intent.ACTION_VIEW, log);
                Intent choose = Intent.createChooser(implicit, "Log in Queue House with:");
                startActivity(choose);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.GRAY);
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(cs, 0, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        other.setText(ss);
        other.setMovementMethod(LinkMovementMethod.getInstance());

        TextView forget = findViewById(R.id.forget);
        String text1 = getString(R.string.forget_password);
        SpannableString ss1 = new SpannableString(text1);
        ClickableSpan cs1 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                forgetPW();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.GRAY);
                ds.setUnderlineText(false);
            }
        };

        ss1.setSpan(cs1, 0, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        forget.setText(ss1);
        forget.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void logIn() {
        user = getSharedPreferences("userID", MODE_PRIVATE);
        SharedPreferences.Editor editor = user.edit();
        editor.putString("phone", account);
        editor.apply();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void forgetPW() {
        Intent intent = new Intent(this, ForgetActivity.class);
        startActivity(intent);
    }


    @Override
    public void onPause() {
        super.onPause();
        CheckBox box = findViewById(R.id.checkBox);
        SharedPreferences.Editor editor = sp.edit();
        if(box.isChecked()){
            editor.putString("account", account);
            editor.putString("password", pw);
        }else{
            editor.clear();
        }
        editor.apply();
    }
}