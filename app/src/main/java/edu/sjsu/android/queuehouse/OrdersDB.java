package edu.sjsu.android.queuehouse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class OrdersDB extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ordersDatabase";
    private static final int VERSION = 1;
    private static final String TABLE_NAME = "orders";
    protected static final String USERID = "_uid";
    private static final String NAME = "name";
    private static final String HOURS = "hours";
    private static final String COST = "cost";


    static final String CREATE_TABLE =
            " CREATE TABLE " + TABLE_NAME +
                    " ("+ USERID + " TEXT NOT NULL, "
                    + NAME + " TEXT NOT NULL, "
                    + HOURS + " TEXT NOT NULL, "
                    + COST + " TEXT NOT NULL);";
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public OrdersDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public long insert(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        return database.insert(TABLE_NAME, null, contentValues);
    }
    public Cursor getAllOrders(String where, String args[]) {
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_NAME,
                new String[]{USERID, NAME, HOURS,COST},
                where, args, null, null, null);
    }
    public boolean delete(int id){
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(TABLE_NAME,USERID+" = "+ "'"+id+"'",null)>0;
    }
}
