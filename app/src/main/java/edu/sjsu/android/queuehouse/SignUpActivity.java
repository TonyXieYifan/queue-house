package edu.sjsu.android.queuehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends UIActivity {

    private final Uri uri = UserInfoProvider.CONTENT_URI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        EditText phoneInput = findViewById(R.id.phone);
        EditText nameInput = findViewById(R.id.email);
        EditText passwordInput = findViewById(R.id.signUp_password);
        Button signUp = findViewById(R.id.button);


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = phoneInput.getText().toString();
                String name = nameInput.getText().toString();
                String pw = passwordInput.getText().toString();
                if(!phone.equals("") && !pw.equals("")){
                    ContentValues values = new ContentValues();
                    values.put("_phone", phone);
                    values.put("name", name);
                    values.put("password", pw);
                    values.put("balance", "0");
                    new Operations().execute(values);

                }else{
                    Toast.makeText(SignUpActivity.this, "Don't make Input empty", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void signIn() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

    class Operations extends AsyncTask<ContentValues, Void, String> {

        @Override
        protected String doInBackground(ContentValues... contentValues) {
            if(contentValues[0] != null){
                if(getContentResolver().insert(uri, contentValues[0]) == null){
                    return "f";
                }
            }
            return "s";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("f")){
                Toast.makeText(SignUpActivity.this, "This Phone Has Been Signed Up", Toast.LENGTH_LONG).show();
            }else{
                signIn();
            }
        }
    }
}