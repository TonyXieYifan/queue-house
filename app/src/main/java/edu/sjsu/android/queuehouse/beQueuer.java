package edu.sjsu.android.queuehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class beQueuer extends AppCompatActivity {
    private final Uri uri = QueuersProvider.CONTENT_URI;
    private EditText queuerNameInput;
    private EditText queuerPlaceInput;
    private EditText queuerRankInput;
    private EditText queuerPriceInput;
    private EditText queuerDescriptionInput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_be_queuer);
        queuerNameInput = findViewById(R.id.queuerName);
        queuerPlaceInput = findViewById(R.id.queuerPlace);
        queuerRankInput = findViewById(R.id.queuerRank);
        queuerPriceInput = findViewById(R.id.queuerPrice);
        queuerDescriptionInput = findViewById(R.id.queuerDes);

    }

    public void beQueuer(View view){
        if(queuerNameInput.getText().toString().equals("")||queuerPlaceInput.getText().toString().equals("")||queuerRankInput.getText().toString().equals("")||queuerPriceInput.getText().toString().equals("")||queuerDescriptionInput.getText().toString().equals("")){
            Toast.makeText(this,"Please enter all the information above!",Toast.LENGTH_SHORT).show();
            return;
        }
        ContentValues values = new ContentValues();
        values.put("name", queuerNameInput.getText().toString());
        values.put("place", queuerPlaceInput.getText().toString());
        values.put("rank", queuerRankInput.getText().toString());
        values.put("price", queuerPriceInput.getText().toString());
        values.put("des", queuerDescriptionInput.getText().toString());
        if (getContentResolver().insert(uri, values) != null) {
            Toast.makeText(this, "Queuer Added", Toast.LENGTH_SHORT).show();
        }
    }
}