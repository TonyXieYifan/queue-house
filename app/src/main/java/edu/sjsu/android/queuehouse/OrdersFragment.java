package edu.sjsu.android.queuehouse;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class OrdersFragment extends Fragment {

    private RecyclerView recycler;
    private SharedPreferences sp;

    ArrayList<Queuer> input = new ArrayList<>();
    private Uri uri = OrdersProvider.CONTENT_URI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sp = getActivity().getSharedPreferences("userID", Context.MODE_PRIVATE);
        String phone = sp.getString("phone", null);
        try (Cursor c = getActivity().getContentResolver().query(uri, null, "_uid =?", new String[]{phone}, null)) {
            input.clear();
            if (c.moveToFirst()) {
                do {
                    int image = R.drawable.logo;
                    switch (c.getString(1).toString()){
                        case "Xinkeng Zheng":
                            image = R.drawable.ken;
                            break;

                        case "SKT T1 Faker":
                            image = R.drawable.faker;
                            break;

                        case "Simple":
                            image = R.drawable.simple;
                            break;

                        case "UZI":
                            image = R.drawable.uzi;
                            break;

                        case "Doublelift":
                            image = R.drawable.doublelift;
                            break;
                        case "Tony Xie":
                            image = R.drawable.leagueoflegend;
                            break;
                        case "Zehui Zhan":
                            image = R.drawable.james;
                            break;
                    }
                    Queuer item = new Queuer(c.getString(1),image,c.getString(2),c.getString(3));
                    input.add(item);
                } while (c.moveToNext());
            }
        }
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders,
                container, false);
        recycler = view.findViewById(R.id.recyclerView);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(view.getContext()));
        RecyclerView.Adapter<OrdersAdapter.ViewHolder> orderAdapter = new OrdersAdapter(input);
        recycler.setAdapter(orderAdapter);
        return view;
    }
}